import wepy from 'wepy'
import '@/baseAPI.js'
export default class extends wepy.mixin {
  data = {
    goods_id: 0,
    goodsDetail: {},
    tabActive: '',
    addressInfo: ''
  }

  methods = {
    // 选择收货地址
    async chooseAddress() {
      const res = await wepy.chooseAddress()

      if (res.errMsg !== 'chooseAddress:ok') {
        return wepy.baseToast('获取收货地址失败！')
      }

      this.addressInfo = res
      wepy.setStorageSync('address', res)
      this.$apply()
    },

    // 添加到购物车
    addToCart() {
      this.$parent.addGoodsCart(this.goodsDetail)

      wepy.showToast({
        title: '已添加至购物车',
        icon: 'success'
      });
      
    }
  }

  computed = {
    addressStr() {
      if (this.addressInfo === null) {
        return '请选择收货地址'
      }
      const add = this.addressInfo
      const str = add.provinceName + add.cityName + add.countyName + add.detailInfo
      return str
    },
    total() {
      return this.$parent.globalData.total
    }
  }

  onLoad(options) {
    this.goods_id = options.goods_id
    this.getGoodsDetail()
    this.addressInfo = wepy.getStorageSync('address')
  }

  async getGoodsDetail() {
    let { data: res } = await wepy.get('/goods/detail?goods_id=' + this.goods_id)
    if (res.meta.status !== 200) {
      return wepy.baseToast(res.meta.msg)
    }
    this.goodsDetail = res.message
    this.$apply()
  }
}