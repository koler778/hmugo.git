import wepy from 'wepy'
import '@/baseAPI.js'

export default class extends wepy.mixin {
  config = {}
  data = {
    // 轮播图数据
    swiperList: [],
    // 分类数据
    cateList: [],
    // floor
    floorData: []
  }
  methods = {
    // 跳转到商品列表
    goGoodsList(url) {
      wepy.navigateTo({ url: url })
    }
  }

  onLoad() {
    this.getSwiperList()
    this.getCateList()
    this.getFloorData()
  }

  // 获取轮播图数据
  async getSwiperList() {
    const { data: res } = await wepy.get('/home/swiperdata')

    if (res.meta.status !== 200) {
      return wepy.baseToast(res.meta.msg)
    }
    this.swiperList = res.message
    // 调用后更新数据
    this.$apply()
  }

  // 获取分类数据
  async getCateList() {
    const { data: res } = await wepy.get('/home/catitems')

    if (res.meta.status !== 200) {
      return wepy.baseToast(res.meta.msg)
    }
    this.cateList = res.message
    this.$apply()
  }

  // 获取 floor 数据
  async getFloorData() {
    const { data: res } = await wepy.get('/home/floordata')
    if (res.meta.status !== 200) {
      return wepy.baseToast(res.meta.msg)
    }
    this.floorData = res.message
    this.$apply()
  }
}