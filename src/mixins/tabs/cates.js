import wepy from 'wepy'
import '@/baseAPI.js'

export default class extends wepy.mixin {
  config = {}
  data = {
    // 当前选中
    active: 0,
    // 分类列表数据
    catesList: [],
    secondList: [],
    // 窗口可用高度
    wh: 0
  }
  methods = {
    // 切换一级分类
    onChange(e) {
      this.secondList = this.catesList[e.detail].children
    },
    // 跳转 goodslist
    goGoodsList(cid) {
      wepy.navigateTo({
        url: '/pages/goods_list?&cid=' + cid
      });

    }
  }

  onLoad() {
    this.getCatesList()
    this.getWindowHeight()
  }

  // 获取分类列表
  async getCatesList() {
    let { data: res } = await wepy.get('/categories')

    if (res.meta.status !== 200) {
      return wepy.baseToast(res.meta.msg)
    }
    this.catesList = res.message
    // 调用后更新数据
    this.$apply()
    this.secondList = this.catesList[0].children
    this.$apply()
  }

  // 获取窗口可用高度
  getWindowHeight() {
    try {
      const res = wx.getSystemInfoSync()
      this.wh = res.windowHeight
    } catch (e) {
    }
  }


}