import wepy from 'wepy'
import '@/baseAPI.js'
export default class extends wepy.mixin {
  data = {
    // 查询关键词
    query: '',
    // 商品分类id
    cid: '',
    total: 0,
    pagenum: 1,
    pagesize: 10,
    goodsList: [],
    isover: false,
    isloading: false
  }
  methods = {
    // 跳转至商品详情页
    goGoodsDetail(goods_id = '') {
      wepy.navigateTo({
        url: '/pages/goods_detail/main?goods_id=' + goods_id
      });

    }
  }

  onLoad(options) {
    this.query = options.query || ''
    this.cid = options.cid || ''
    this.getGoodsList()
  }

  // 上拉加载
  onReachBottom() {
    if (this.pagenum * this.pagesize >= this.total) {
      this.isover = true
      return
    }
    this.getGoodsList()
  }

  // 下拉加载
  onPullDownRefresh() {
    this.pagenum = 1
    this.goodsList = []
    this.isloading = false
    this.isover = false
    this.getGoodsList(function () {
      wepy.stopPullDownRefresh()
    })
  }

  // 获取
  async getGoodsList(cb) {
    if (this.isloading) {
      return
    }
    this.isloading = true
    let { data: res } = await wepy.get('/goods/search', {
      query: this.query,
      cid: this.cid,
      pagenum: this.pagenum++,
      pagesize: this.pagesize
    })
    this.isloading = false
    if (res.meta.status !== 200) {
      return wepy.baseToast(res.meta.msg)
    }
    this.total = res.message.total
    this.goodsList = [...this.goodsList, ...res.message.goods]
    this.$apply()

    cb && cb()
  }


}