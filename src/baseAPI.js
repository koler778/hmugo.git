import wepy from 'wepy'

// 请求根地址
const baseURL = 'https://www.zhengzhicheng.cn/api/public/v1'

/**
 * 消息提示 - 一般用于错误提示
 */
wepy.baseToast = function (msg = '获取数据失败') {
  return wepy.showToast({
    title: msg,
    icon: 'none',
    duration: 2000
  })
}

wepy.get = function (url, data={}) {
  return wepy.request({
    url: baseURL + url,
    method: 'GET',
    data: data
  })
}

wepy.post = function (url, data={}) {
  return wepy.request({
    url: baseURL + url,
    method: 'POST',
    data: data
  })
}